
export default [
  {
    path: '/',
    component: () => import('components/CrudUser')
  },
  {
    path: '/crud2',
    component: () => import('components/cruduser/CrudUser')
  },
  /*{
    path: '/',
    component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/index') }
    ]
  },*/

  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
