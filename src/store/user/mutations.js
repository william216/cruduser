/*
export const someMutation = (state) => {}
 */
export const ADD_USER=(state,user)=>{
	let id=state.users.length+1
	user.id=id
	state.users.push(user)
	state.user={}
}

export const DELETE_USER=(state, selectedUser)=>{
	let i=0
	selectedUser.forEach(u=>{
		state.users.forEach(user=>{
			if(user.id==u.id){
				state.users.splice(i,1)
			}
			i+=1
		})
		i=0
	})
	
}

export const EDIT_USER=(state,user)=>{
	state.users.forEach(u=>{
		if(user.id==u.id){
			state.user=u
		}
	})
}

export const INIT_USER=(state)=>{
	state.user={
		id:'',
		name:'',
		email:'',
		role:'',
		password:'',
		status:'active',
		avatar:'',
		password:''
	}
}

export const SET_FILTER=(state,filter)=>{
	state.filter=filter
}

export const SET_USER=(state,user)=>{
	state.user=user
	console.log(state.user)
}