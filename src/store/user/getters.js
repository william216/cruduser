/*
export const someGetter = (state) => {}
 */
 /* Liste des utilisateurs*/
export const users = (state) => {
	if(state.filter.role=="" && state.filter.status==""){
		return state.users
	}
	else{
		if(state.filter.role=="" && state.filter.status!=""){
			const result=state.users.filter(user=>{
				return user.status==state.filter.status
			})
			return result
		}
		else if(state.filter.role!="" && state.filter.status==""){
			const result=state.users.filter(user=>{
				return user.role==state.filter.role
			})
			return result
		}
		else{
			const result=state.users.filter(user=>{
				return (user.role==state.filter.role) && (user.status==state.filter.status)
			})
			return result
		}
		
	}
}

/* Nombre d'utilisateur*/
export const usersCount=(state)=>{return state.users.length}

/* utilisateur*/
export const user=(state)=>{return state.user}

/*Utilisateur à supprimer*/
//export const selectedSecond=(state)=>{return state.selectedSecond}

export const filter=(state)=>{return state.filter}

