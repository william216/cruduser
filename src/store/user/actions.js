/*
export const someAction = (state) => {}
 */
export const addUser=(state,user)=>{
	state.commit('ADD_USER',user)
}

export const deleteUser=(state,selectedUser)=>{
	state.commit('DELETE_USER',selectedUser)
}

export const editUser=(state,user)=>{
	state.commit('EDIT_USER',user)
}

export const initUser=(state)=>{
	state.commit('INIT_USER')
}

export const setUser=(state,user)=>{
	state.commit('SET_USER',user)
}

export const setFilter=(state,filter)=>{
	state.commit('SET_FILTER',filter)
}